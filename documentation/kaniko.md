# Kaniko

This template builds an image from Dockerfile using Kaniko. It publishes the image in your repo's container registry.

## Usage

```yaml
include:
    - project: 'brandonbutler/templates'
      file: '/templates/kaniko.yml'

stages:
    - package

kaniko-build:
    extends: .kaniko-build
    stage: package
```

## Automatic tagging strategy

This job will automatically set tags for the image using a case-statement. Assuming a repo name like `myname/myrepo` it will create an image tagged `registry.gitlab.com/myname/myrepo/mybranchname:$CI_COMMIT_SHA` on non-default branches, and `registry.gitlab.com/myname/myrepo:latest` on the default branch.
